var reservas = new Vue({
  el: "#reservas",
  data: {
    values: []
  },
  methods: {
    cargarDatos: function(datos){
      this.values = datos;
    },
    deleteValue: function(dato){
      this.values = tm.deleteValue(dato);
      tm.setValues();
    },
    obtenerHora: function(fecha){
      return moment(fecha).format('HH:mm');
    }
  }
});

var tm = new Vue({
  el: "#timeline",
  data: {
    datos: "",
    container: "",
    items: "",
    options: "",
    timeline: "",
    rangeValue: 0,
    ultimoId: 2,
    moveValue: 0.8
  },
  created: function(){
    this.datos = [
      {
        "id": 1,
        "content": "Reserva 1",
        "start": "2021-01-17 10:00",
        "end": "2021-01-17 10:30"
      },
      {
        "id": 2,
        "content": "Reserva 2",
        "start": "2021-01-17 11:00",
        "end": "2021-01-17 11:45"
      }
    ];
    this.options = {
      orientation: {
        axis: "top",
        item: "top"
      },
      showMajorLabels: false,
      autoResize: false,
      timeAxis: {scale: 'minute', step: 15},
      zoomMax: 10000000, // 10,000 years is maximum possible
      zoomMin: 10000000 // 10ms
    };
  },
  mounted: function(){
    this.setValues();
    reservas.cargarDatos(this.datos);
  },
  methods: {
    move: function(percentage){
      var range = this.timeline.getWindow();
      var interval = range.end - range.start;
      this.timeline.setWindow({
        start: range.start.valueOf() - interval * percentage,
        end: range.end.valueOf() - interval * percentage
      });
    },
    setValues: function(){
      $("#visualization").empty();
      this.container = document.getElementById("visualization");
      this.items = new vis.DataSet(this.datos);
      this.timeline = new vis.Timeline(this.container, this.items, this.options);
      this.timeline.focus(this.datos[this.datos.length - 1].id, {zoom: true});
    },
    moveLeft: function(){
      this.move(this.moveValue);
    },
    moveRight: function(){
      this.move(-this.moveValue);
    },
    fitting: function(){
      this.rangeValue = 0;
      this.timeline.fit(this.items.getIds());
    },
    addValue: function(nombre, fecha, desde, hasta){
      this.ultimoId = this.ultimoId + 1;
      this.datos.push({
        "id": this.ultimoId,
        "content": nombre,
        "start": fecha + " " + desde,
        "end": fecha + " " + hasta
      });
      this.setValues();
    },
    deleteValue: function(value){
      for(var i = 0; i < this.datos.length; i++) {
          if (this.datos[i].id === value) {
              this.datos.splice(i, 1);
              break;
          }
      }
      return this.datos;      
    }
  }
});

var form = new Vue({
  el: '#timeline-form',
  data: {
    title: 'RESERVAR',
    nombre: '',
    fecha: moment().format('YYYY-MM-DD'),
    desde: moment().format('HH:mm'),
    hasta: moment().add(1, 'hours').format('HH:mm')
  },
  methods: {
    validarData: function(){
      if(this.nombre != "" && this.fecha != "" && this.desde != "" && this.hasta != "")
        return true;
      else
        return false;
    },
    agregarValor: function(){
      if(this.validarData())
        tm.addValue(this.nombre, this.fecha, this.desde, this.hasta);
      else
        alert("Debe completar todos los campos.");
    }
  } 
});




